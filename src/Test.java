
public class Test {

	public static void main(String[] args) {
		StringsHashMap map = new StringsHashMap();
		
		System.out.println(map.size());					// Expected 0
		System.out.println(map.put("foo", "bar"));		// Expected null
		System.out.println(map.size());					// Expected 1
		System.out.println(map.put("Foo", "Bar"));		// Expected null
		System.out.println(map.size());					// Expected 2
		System.out.println(map.put("FOO", "BAR"));		// Expected null
		System.out.println(map.size());					// Expected 3
		System.out.println(map.put("foo", "_bar_"));	// Expected "bar"
		System.out.println(map.size());					// Expected 3
		
		System.out.println(map.get("foo"));				// Expected "_bar_"
		System.out.println(map.get("Foo"));				// Expected "Bar"
		System.out.println(map.get("FOO"));				// Expected "BAR"
		System.out.println(map.get("Bar"));				// Expected null
		
		System.out.println(map.containsKey("foo"));		// Expected true
		System.out.println(map.containsKey("Foo"));		// Expected true
		System.out.println(map.containsKey("FOO"));		// Expected true
		System.out.println(map.containsKey("Bar"));		// Expected false
		
		System.out.println(map.remove("Foo"));			// Expected "Bar"
		System.out.println(map.size());					// Expected 2
		System.out.println(map.containsKey("Foo"));		// Expected false
		System.out.println(map.get("Foo"));				// Expected null
		
		System.out.println(map.remove("FOO"));			// Expected "BAR"
		System.out.println(map.size());					// Expected 1
		System.out.println(map.containsKey("FOO"));		// Expected false
		System.out.println(map.get("FOO"));				// Expected null
	}
}
