public class StringsHashMap {
	private final Node[] buckets = new Node[16];
	private final int bucketSize = Integer.MAX_VALUE / buckets.length;	// the bucketSize is the max hash code divided by number of buckets
	private int size = 0;
	
	/**
	 * Returns the current size of the StringsHashMap
	 * @return current size
	 */
	public int size() {
		return this.size;
	}

	/**
	 * Adds or updates the StringsHashMap with the given key-value pair
	 * @param key - a String representing the key
	 * @param value - a String representing the value
	 * @return the old value associated with the key or null if no old value found
	 */
	public String put(String key, String value) {
		int bucketIndex = this.getBucketIndex(key.hashCode());	// Get bucket index
		Node bucket = buckets[bucketIndex];						// Get bucket
		if (bucket == null) {
			// Bucket not found --> create new bucket node
			buckets[bucketIndex] = bucket = new Node(key, value, null);
			size++;												// Increase size counter
			return null;										// No previous value --> return null
		} else {
			// Bucket found
			Node existing = bucket.getNode(key);				// Get matching node
			if (existing != null) {
				// Matching node found (not null)
				String oldValue = existing.value;				// Save old value
				existing.value = value;							// Update value
				return oldValue;								// Return old value
			} else {
				// Matching node not found (null)
				Node newNode = new Node(key, value, bucket);	// Create new node, link it to current bucket
				buckets[bucketIndex] = newNode;					// Replace bucket with new node
				this.size++;									// Increase size counter
				return null;									// No previous value --> return null
			}
		}
	}

	/**
	 * Gets the value associated with the key or null if not found
	 * @param key - a String representing the key
	 * @return a String representing the value associated with the key or null if not found.
	 * This method intentionally returns null if not found instead of throwing an exception to simplify the tests
	 */
	public String get(String key) {
		int bucketIndex = this.getBucketIndex(key.hashCode());	// Get bucket index
		Node bucket = buckets[bucketIndex];						// Get bucket
		if (bucket == null) return null;						// Bucket does not exist --> not found
		Node match = bucket.getNode(key);						// Find matching node
		return match == null ? null : match.value;				// Return matching node's value or null if not found
	}

	/**
	 * Checks if a key is present in the StringsHashMap
	 * @param key - a String representing the key
	 * @return true if found otherwise false
	 */
	public Boolean containsKey(String key) {
		int bucketIndex = this.getBucketIndex(key.hashCode());	// Get bucket index
		Node bucket = buckets[bucketIndex];						// Get bucket
		if (bucket == null) return false;						// Bucket does not exist --> not found
		return bucket.getNode(key) != null;						// Find matching node --> return true if found (not null)
	}

	/**
	 * Removes a key-value pair from the StringsHashMap
	 * @param key - a String representing the key to remove
	 * @return a String representing the value associated with the key or null if not found
	 */
	public String remove(String key) {
		int bucketIndex = this.getBucketIndex(key.hashCode());	// Get bucket index
		Node bucket = buckets[bucketIndex];						// Get bucket
		if (bucket == null) return null;						// Bucket does not exist --> not found
		if (bucket.key == key) {
			// Bucket is the matching node
			buckets[bucketIndex] = bucket.next;					// Remove bucket (set bucket pointer to next node in the linked list)
			size--;												// Decrease size counter
			return bucket.value;								// Return value of removed bucket
		}
		
		Node linkingNode = bucket.getLinkingNode(key);			// Bucket is not a direct match --> get linking node
		if (linkingNode == null) return null;					// Linking node not found --> not found
		
		// Linking node found
		Node removed = linkingNode.next;						// Save removed node
		linkingNode.next = removed.next;						// Remove node (set linking node's pointer to next node in the sub-tree
		size--;													// Decrease size counter
		return removed.value;									// Return value of removed bucket
	}
	
	/**
	 * Calculates the bucket index based on the hash
	 * @param hash - an int representing the hash of the key
	 * @return an int representing the bucket index to use
	 */
	private int getBucketIndex(int hash) {
		return hash / this.bucketSize;
	}

	/**
	 * A node class that holds the key-value pair
	 */
	private class Node {
		public final String key;
		public String value;
		public Node next = null;
		
		public Node(String key, String value, Node next) {
			this.key = key;
			this.value = value;
			this.next = next;
		}
		
		/**
		 * Gets a Node in the sub-tree (current node or sub-nodes) that matches the key
		 * @param key - a String representing the key
		 * @return a matched Node or null
		 */
		public Node getNode(String key) {
			if (this.key == key) return this;		// This Node matches the key --> return this Node (matching node)
			if (this.next == null) return null;		// This Node has no sub-nodes --> not found	
			return this.next.getNode(key);			// Keep searching sub-nodes
		}
		
		/**
		 * Gets a Node in the sub-tree (current node or sub-nodes) that holds a link to a matched Node
		 * @param key - a String representing the key
		 * @return a Node that is linking to a Node that matches the key or null if such Node is not found
		 */
		public Node getLinkingNode(String key) {
			if (this.next == null) return null;		// This Node does not link to any Node --> not found
			if (this.next.key == key) return this;	// This Node links to the searched Node --> return this Node (linking node)
			return this.next.getLinkingNode(key);	// Keep searching sub-nodes
		}
	}	
}
